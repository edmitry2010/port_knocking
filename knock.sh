#!/bin/bash

# Запуск скрипта.
# В качестве параметра, надо указать адрес сервера.
# ./knock.sh 10.100.0.15		 - это вариант, если указан полный путь до шифрованного файла
# ./knock.sh 10.100.0.15 /root/knock.gpg - это вариант, если передаёте полный путь до шифрованного файла в качестве параметра
# ./knock.sh 10.100.0.15 /root/.knock	 - это вариант, если передаёте полный путь до папочки с файлами *.gpg

# for x in 7000 8000 9000 10000; do nmap -Pn --max-retries 0 -p $x 10.100.0.15; done
# for x in 7000 8000 9000 10000; do nc -zw 0.1 10.100.0.15 $x; done

# Для того, что бы в истории небыло записей на какие порты вы стучитесь, надо создать файл с портами.
# nano knock
# 7000 8000 9000 10000

# Потом зашифровать его обычным паролем, через gpg
# gpg -c knock && rm -f knock
# Файл knock, следует удалить и оставить только зашифрованную версию knock.gpg

# Если потребуется расшифровать
# gpg -d knock.gpg 2> /dev/null > knock

# Что бы вам каждый раз не указывать полный путь до скрипта, можно сделать симлинк.
# Например такой.
# В вашем случае, указывайте свой полный путь.
# ln -s /etc/myserver/git/port_knocking/knock.sh /bin/knock
# После этого, можно будет вызывать скрипт из любого места в системе.
# knock 10.100.0.15
# sudo knock 10.100.0.15 /root/.knock/knock.gpg
# sudo knock 10.100.0.15 /root/.knock

inper=($*)

KnockIP=${inper[0]}

# Определяемся с шифрованным файлом с портами.
if [[ ${inper[1]} ]]; then
  FileKno=${inper[1]}
else
  FileKno='/etc/myserver/git/port_knocking/knock.gpg' # Полный путь до зашифрованного файла с портами.
fi

if [ -d $FileKno ]; then
  GPGdir='yes'
fi

function FileGPG() {
  # Вслучае если указали файл
  if [[ $FileKno ]]; then
    if ! [ -f $FileKno ]; then
      echo "Шифрованного файла с портами, не найдено: $FileKno"
      exit 0
    fi
  else
    echo "Вы не указали шифрованный файл с портами"
    exit 0
  fi
}

function DirGPG() {
  # Вслучае если указали папочку с файлами
  arrFilesGPG=(`ls $FileKno/*.gpg`)
  PS3='Выбирете файл из списка: '
  select SelectFileGPG in ${arrFilesGPG[@]}; do
    FileKno=$SelectFileGPG
    break
  done
}

if [[ $GPGdir == yes ]]; then
  DirGPG
else
  FileGPG
fi

PortsGPG=$(gpg -d $FileKno 2> /dev/null)

if ! [[ $PortsGPG ]]; then
  echo "Вероятно, вы указали неправильный шифрованный файл: $FileKno"
  exit 0
fi

# Вариант 1 (nmap)
#for x in $PortsGPG; do nmap -Pn --max-retries 0 -p $x $KnockIP > /dev/null; done

# Вариант 2 (nc)
# 1 - это время ожидания ответа
for x in $PortsGPG; do nc -zw 1 $KnockIP $x > /dev/null; done
